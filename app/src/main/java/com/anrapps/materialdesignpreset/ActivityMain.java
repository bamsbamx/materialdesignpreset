package com.anrapps.materialdesignpreset;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.anrapps.materialdesignpreset.utils.UIUtils;
import com.facebook.drawee.backends.pipeline.Fresco;


public class ActivityMain extends BaseActivity {

	private View mToolbarContainer;
    private RecyclerView mRecyclerView;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();
        Fresco.initialize(this);

		//For API Levels prior to 21
        mToolbarContainer = findViewById(R.id.toolbar_actionbar_container);

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        String[] items = new String[20];

        for (int i = 0; i < 20; i++) items[i] = ("http://lorempixel.com/500/500/sports/" + i);

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        AdapterMain mAdapter = new AdapterMain(items);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setOnScrollListener(new UIUtils.ScrollManager(
                mToolbarContainer != null ? mToolbarContainer : getActionBarToolbar()));
        mRecyclerView.setAdapter(mAdapter);

        UIUtils.setToolbarTopPadding(mRecyclerView);
        UIUtils.setRefreshLayoutTopOffset(swipeRefreshLayout);
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_FAVOURITES;
    }

    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        return mRecyclerView.canScrollVertically(-1);
    }

	@Override
	protected void onNavDrawerStateChanged(boolean isOpen) {
		if (isOpen) 
			UIUtils.setActionBarVisibility(
					mToolbarContainer != null ? mToolbarContainer : getActionBarToolbar(),
					true);
	}
	
	

    @Override
    protected void requestDataRefresh() {
        mHandler.post(new Runnable() {
            @Override public void run() {
                onRefreshingStateChanged(true);
                Toast.makeText(getApplicationContext(), "START", Toast.LENGTH_SHORT).show();
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onRefreshingStateChanged(false);
                Toast.makeText(getApplicationContext(), "FINISH", Toast.LENGTH_SHORT).show();
            }
        }, 1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
