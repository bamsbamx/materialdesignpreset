package com.anrapps.materialdesignpreset;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

public class AdapterMain extends RecyclerView.Adapter<AdapterMain.ViewHolder> {

    private String[] mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public SimpleDraweeView mSimpleDraweeView;
        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.listitem_main_text);
            mSimpleDraweeView = (SimpleDraweeView) v.findViewById(R.id.listitem_main_image);
        }
    }

    public AdapterMain(String[] myDataset) {
        mDataset = myDataset;
    }

    @Override
    public AdapterMain.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_main, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(getItem(position));
        holder.mSimpleDraweeView.setImageURI(Uri.parse(getItem(position)));

    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public String getItem(int position) {
        return mDataset[position];
    }
}