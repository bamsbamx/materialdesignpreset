package com.anrapps.materialdesignpreset.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import com.anrapps.materialdesignpreset.R;
import android.support.v7.widget.Toolbar;

public class UIUtils {

    public static final int HEADER_HIDE_ANIM_DURATION = 200;

    private static final int[] RES_IDS_ACTION_BAR_SIZE = { R.attr.actionBarSize };

    public static void setToolbarTopPadding(View v) {
        int topPadding = calculateActionBarSize(v.getContext());
        v.setPadding(v.getPaddingLeft(), v.getPaddingTop() + topPadding,
                v.getPaddingRight(), v.getPaddingBottom());
    }

    public static void setRefreshLayoutTopOffset(SwipeRefreshLayout swipeRefreshLayout) {
        int progressBarStartMargin = swipeRefreshLayout.getContext().getResources().getDimensionPixelSize(
                R.dimen.swipe_refresh_progress_bar_start_margin);
        int progressBarEndMargin = swipeRefreshLayout.getContext().getResources().getDimensionPixelSize(
                R.dimen.swipe_refresh_progress_bar_end_margin);
        int top = calculateActionBarSize(swipeRefreshLayout.getContext());
        swipeRefreshLayout.setProgressViewOffset(false,
                top + progressBarStartMargin, top + progressBarEndMargin);
    }

    private static int calculateActionBarSize(Context context) {
        if (context == null) return 0;

        Resources.Theme curTheme = context.getTheme();
        if (curTheme == null) return 0;

        TypedArray att = curTheme.obtainStyledAttributes(RES_IDS_ACTION_BAR_SIZE);
        if (att == null) return 0;

        float size = att.getDimension(0, 0);
        att.recycle();
        return (int) size;
    }

    //TODO: Do not perform any anim if already animating
    public static class ScrollManager extends RecyclerView.OnScrollListener {

        private View mHeaderView;
        private boolean shown = true;

        public ScrollManager(View toolbar) {
            this.mHeaderView = toolbar;
        }

        @Override
        public void onScrolled(RecyclerView r, int dx, int dy) {
            if (!r.canScrollVertically(-1)) {
                setActionBarVisibility(mHeaderView, true);
                return;
            }

            if (Math.abs(dy) < 5) return;
            setActionBarVisibility(mHeaderView, dy < 0);
        }

        private void setActionBarVisibility(View toolbar, boolean visible) {
            if (visible && !shown) {
                //runTranslateAnimation(toolbar, 0, new DecelerateInterpolator());
				UIUtils.setActionBarVisibility(toolbar, visible);
                shown = true;
            } else if (!visible && shown) {
                //runTranslateAnimation(toolbar, -toolbar.getBottom(), new AccelerateInterpolator());
				UIUtils.setActionBarVisibility(toolbar, visible);
                shown = false;
            }
        }

        //private void runTranslateAnimation(View view, int translateY, Interpolator interpolator) {
        //    Animator slideInAnimation = ObjectAnimator.ofFloat(view, "translationY", translateY);
        //    slideInAnimation.setDuration(HEADER_HIDE_ANIM_DURATION);
        //    slideInAnimation.setInterpolator(interpolator);
        //    slideInAnimation.start();
        //}
    }
	
	public static void setActionBarVisibility(View toolbar, boolean visible) {
		final int translateY = visible ? 0 : -toolbar.getBottom();
		Animator slideInAnimation = ObjectAnimator.ofFloat(toolbar, "translationY", translateY);
		slideInAnimation.setDuration(HEADER_HIDE_ANIM_DURATION);
		slideInAnimation.setInterpolator(visible ? new DecelerateInterpolator() : new AccelerateInterpolator());
		slideInAnimation.start();
	}
}
