package com.anrapps.materialdesignpreset;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anrapps.materialdesignpreset.utils.PrefUtils;
import com.anrapps.materialdesignpreset.widget.MultiSwipeRefreshLayout;
import com.anrapps.materialdesignpreset.widget.ScrimInsetsScrollView;

import java.util.ArrayList;

public abstract class BaseActivity extends ActionBarActivity implements
        MultiSwipeRefreshLayout.CanChildScrollUpCallback {

    private DrawerLayout mDrawerLayout;

    private Handler mHandler;

    protected static final int NAVDRAWER_ITEM_FAVOURITES = 0;
    protected static final int NAVDRAWER_ITEM_SETTINGS = 1;
    protected static final int NAVDRAWER_ITEM_INVALID = -1;
    protected static final int NAVDRAWER_ITEM_SEPARATOR = -2;

    private static final int[] NAVDRAWER_TITLE_RES_ID = new int[]{
            R.string.navdrawer_item_favourites,
            R.string.navdrawer_item_settings
    };
    private static final int[] NAVDRAWER_ICON_RES_ID = new int[] {
            R.drawable.ic_drawer_favourites,
            R.drawable.ic_drawer_settings
    };
    private static final int NAVDRAWER_LAUNCH_DELAY = 250;

    private static final int MAIN_CONTENT_FADEOUT_DURATION = 150;
    private static final int MAIN_CONTENT_FADEIN_DURATION = 250;

    private ArrayList<Integer> mNavDrawerItems = new ArrayList<>();

    private View[] mNavDrawerItemViews = null;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private Toolbar mActionBarToolbar;
	
	@Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        getActionBarToolbar();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHandler = new Handler();

        ActionBar ab = getSupportActionBar();
        if (ab != null) ab.setDisplayHomeAsUpEnabled(true);
    }
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setupNavDrawer();

        trySetupSwipeRefresh();

        View mainContent = findViewById(R.id.main_content);
        if (mainContent != null) {
            mainContent.setAlpha(0);
            mainContent.animate().alpha(1).setDuration(MAIN_CONTENT_FADEIN_DURATION);
        }
    }
	
	@Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) closeNavDrawer();
        else super.onBackPressed();
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case R.id.menu_about:
//                HelpUtils.showAbout(this);
//                return true;
//
//            case R.id.menu_map:
//                startActivity(new Intent(this, UIUtils.getMapActivityClass(this)));
//                finish();
//                break;
//        }
        return super.onOptionsItemSelected(item);
    }
	
	@Override
    public boolean canSwipeRefreshChildScrollUp() {
        return false;
    }

    
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_INVALID;
    }
	
    protected void onNavDrawerStateChanged(boolean isOpen) {}

    protected boolean isNavDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.START);
    }

    protected void closeNavDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(Gravity.START);
        }
    }

      

    protected void requestDataRefresh() {}

    


    protected Toolbar getActionBarToolbar() {
        if (mActionBarToolbar == null) {
            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (mActionBarToolbar != null) 
                setSupportActionBar(mActionBarToolbar);
        }
        return mActionBarToolbar;
    }
    

    protected void onRefreshingStateChanged(boolean refreshing) {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(refreshing);
        }
    }

    protected void enableDisableSwipeRefresh(boolean enable) {
        if (mSwipeRefreshLayout != null) 
            mSwipeRefreshLayout.setEnabled(enable);
    }
	
	private void trySetupSwipeRefresh() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeResources(
				R.color.refresh_progress_1,
				R.color.refresh_progress_2,
				R.color.refresh_progress_3);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
					@Override public void onRefresh() {
						requestDataRefresh();
					}
				});

            if (mSwipeRefreshLayout instanceof MultiSwipeRefreshLayout) {
                MultiSwipeRefreshLayout mswrl = (MultiSwipeRefreshLayout) mSwipeRefreshLayout;
                mswrl.setCanChildScrollUpCallback(this);
            }
			
			enableDisableSwipeRefresh(true);
        }
    }
	
	
	private void setupNavDrawer() {
        int selfItem = getSelfNavDrawerItem();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout == null) return;

        mDrawerLayout.setStatusBarBackgroundColor(
			getResources().getColor(R.color.theme_primary_dark));
        ScrimInsetsScrollView navDrawer = (ScrimInsetsScrollView)
			mDrawerLayout.findViewById(R.id.navdrawer);
        if (selfItem == NAVDRAWER_ITEM_INVALID) {
            if (navDrawer != null) ((ViewGroup) navDrawer.getParent()).removeView(navDrawer);
            mDrawerLayout = null;
            return;
        }

        if (navDrawer != null) {
//            final View chosenAccountContentView = findViewById(R.id.chosen_account_content_view);
            final View chosenAccountView = findViewById(R.id.navdrawer_header);
            final int navDrawerChosenAccountHeight = getResources().getDimensionPixelSize(
				R.dimen.navdrawer_header_height);
            navDrawer.setOnInsetsCallback(new ScrimInsetsScrollView.OnInsetsCallback() {
					@Override
					public void onInsetsChanged(Rect insets) {
//                    ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)
//                            chosenAccountContentView.getLayoutParams();
//                    lp.topMargin = insets.top;
//                    chosenAccountContentView.setLayoutParams(lp);

						ViewGroup.LayoutParams lp2 = chosenAccountView.getLayoutParams();
						lp2.height = navDrawerChosenAccountHeight + insets.top;
						chosenAccountView.setLayoutParams(lp2);
					}
				});
        }

        if (mActionBarToolbar != null) {
            mActionBarToolbar.setNavigationIcon(R.drawable.ic_drawer);
            mActionBarToolbar.setNavigationOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						mDrawerLayout.openDrawer(Gravity.START);
					}
				});
        }

        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
				@Override public void onDrawerClosed(View drawerView) {
					onNavDrawerStateChanged(false); }
				@Override public void onDrawerOpened(View drawerView) {
					onNavDrawerStateChanged(true); }
				@Override public void onDrawerStateChanged(int newState) {
					onNavDrawerStateChanged(isNavDrawerOpen()); }
				@Override public void onDrawerSlide(View drawerView, float slideOffset) {}
			});

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);

        populateNavDrawer();

        if (!PrefUtils.isWelcomeDone(this)) {
            PrefUtils.markWelcomeDone(this);
            mDrawerLayout.openDrawer(Gravity.START);
        }
    }
	
	
	
	private View makeNavDrawerItem(final int itemId, ViewGroup container) {
        boolean selected = getSelfNavDrawerItem() == itemId;
        int layoutToInflate;
        if (itemId == NAVDRAWER_ITEM_SEPARATOR) {
            layoutToInflate = R.layout.navdrawer_separator;
        } else {
            layoutToInflate = R.layout.navdrawer_item;
        }
        View view = getLayoutInflater().inflate(layoutToInflate, container, false);

        if (isSeparator(itemId)) {
            // we are done
            view.setClickable(false);
            view.setFocusable(false);
            view.setContentDescription("");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                view.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
            }
            return view;
        }

        ImageView iconView = (ImageView) view.findViewById(R.id.navdrawer_item_icon);
        TextView titleView = (TextView) view.findViewById(R.id.navdrawer_item_title);
        int iconId = itemId >= 0 && itemId < NAVDRAWER_ICON_RES_ID.length ?
			NAVDRAWER_ICON_RES_ID[itemId] : 0;
        int titleId = itemId >= 0 && itemId < NAVDRAWER_TITLE_RES_ID.length ?
			NAVDRAWER_TITLE_RES_ID[itemId] : 0;

        // set icon and text
        iconView.setVisibility(iconId > 0 ? View.VISIBLE : View.GONE);
        if (iconId > 0) {
            iconView.setImageResource(iconId);
        }

        titleView.setText(getString(titleId));

        formatNavDrawerItem(view, itemId, selected);

        view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onNavDrawerItemClicked(itemId);
				}
			});

        return view;
    }
	
	private void setSelectedNavDrawerItem(int itemId) {
        if (mNavDrawerItemViews != null) {
            for (int i = 0; i < mNavDrawerItemViews.length; i++) {
                if (i < mNavDrawerItems.size()) {
                    int thisItemId = mNavDrawerItems.get(i);
                    formatNavDrawerItem(mNavDrawerItemViews[i], thisItemId, itemId == thisItemId);
                }
            }
        }
    }
	
	private void formatNavDrawerItem(View view, int itemId, boolean selected) {
        if (isSeparator(itemId)) return;

        ImageView iconView = (ImageView) view.findViewById(R.id.navdrawer_item_icon);
        TextView titleView = (TextView) view.findViewById(R.id.navdrawer_item_title);

        titleView.setTextColor(selected ?
							   getResources().getColor(R.color.navdrawer_text_color_selected) :
							   getResources().getColor(R.color.navdrawer_text_color));
        iconView.setColorFilter(selected ?
								getResources().getColor(R.color.navdrawer_icon_tint_selected) :
								getResources().getColor(R.color.navdrawer_icon_tint));
    }
	
	private void goToNavDrawerItem(int item) {
//        Intent intent;
        switch (item) {
            case NAVDRAWER_ITEM_FAVOURITES:
//                intent = new Intent(this, MyScheduleActivity.class);
//                startActivity(intent);
//                finish();
                break;
            case NAVDRAWER_ITEM_SETTINGS:
//                intent = new Intent(this, BrowseSessionsActivity.class);
//                startActivity(intent);
//                finish();
                break;
        }
    }
	
	private void populateNavDrawer() {

        mNavDrawerItems.clear();
        mNavDrawerItems.add(NAVDRAWER_ITEM_FAVOURITES);
		mNavDrawerItems.add(NAVDRAWER_ITEM_SEPARATOR);
        mNavDrawerItems.add(NAVDRAWER_ITEM_SETTINGS);

        createNavDrawerItems();
    }

    private void createNavDrawerItems() {
        ViewGroup drawerItemsListContainer = (ViewGroup) findViewById(R.id.navdrawer_items_list);
        if (drawerItemsListContainer == null) return;

        mNavDrawerItemViews = new View[mNavDrawerItems.size()];
        drawerItemsListContainer.removeAllViews();
        int i = 0;
        for (int itemId : mNavDrawerItems) {
            mNavDrawerItemViews[i] = makeNavDrawerItem(itemId, drawerItemsListContainer);
            drawerItemsListContainer.addView(mNavDrawerItemViews[i]);
            ++i;
        }
    }

    private void onNavDrawerItemClicked(final int itemId) {
        if (itemId == getSelfNavDrawerItem()) {
            mDrawerLayout.closeDrawer(Gravity.START);
            return;
        }

        if (isSpecialItem(itemId)) goToNavDrawerItem(itemId);
        else {
            mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						goToNavDrawerItem(itemId);
					}
				}, NAVDRAWER_LAUNCH_DELAY);

            setSelectedNavDrawerItem(itemId);
            View mainContent = findViewById(R.id.main_content);
            if (mainContent != null)
                mainContent.animate().alpha(0).setDuration(MAIN_CONTENT_FADEOUT_DURATION);
        }

        mDrawerLayout.closeDrawer(Gravity.START);
    }
	
	private boolean isSpecialItem(int itemId) {
        return itemId == NAVDRAWER_ITEM_SETTINGS;
    }

    private boolean isSeparator(int itemId) {
        return itemId == NAVDRAWER_ITEM_SEPARATOR;
    }
	

    
}
